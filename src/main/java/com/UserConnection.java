package com;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class UserConnection {
    static Connection connection;
    public  static Connection getConnection() throws ClassNotFoundException, SQLException {
        if(connection==null) {
            Class.forName("com.mysql.jdbc.Driver");
            connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/test4", "root", "root");
        }
        return connection;

        }
    }

