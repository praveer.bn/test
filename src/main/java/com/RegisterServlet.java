package com;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.*;
@WebServlet("/Register")
public class RegisterServlet extends HttpServlet {
    Connection connection;
    PreparedStatement preparedStatement;
    Statement statement;
    ResultSet resultSet;
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("text/html");
        PrintWriter printWriter=resp.getWriter();
        String fname=req.getParameter("fname");
        String lname=req.getParameter("lname");
        String city=req.getParameter("ucity");
        String email=req.getParameter("uemail");
        String role=req.getParameter("role");
        int id = Integer.parseInt(req.getParameter("uid"));
        int age = Integer.parseInt(req.getParameter("uage"));
        int ph = Integer.parseInt(req.getParameter("uph"));
        try {
            connection=UserConnection.getConnection();
            statement=connection.createStatement();
            resultSet=statement.executeQuery("select * from person where id="+id);
            if(!resultSet.next()){
                preparedStatement = connection.prepareStatement("insert into person values(?,?,?,?,?,?,?,?)");
                preparedStatement.setInt(1, id);
                preparedStatement.setInt(4, ph);
                preparedStatement.setInt(5, age);
                preparedStatement.setString(2, fname);
                preparedStatement.setString(3, lname);
                preparedStatement.setString(6, email);
                preparedStatement.setString(7, city);
                preparedStatement.setString(8, role);
                preparedStatement.executeUpdate();
                RequestDispatcher requestDispatcher = req.getRequestDispatcher("index.jsp");
                printWriter.print("<h1>User Added.....</h1>");
                requestDispatcher.include(req, resp);
            }else{
                RequestDispatcher requestDispatcher=req.getRequestDispatcher("Register.jsp");
                printWriter.print("<h1 style=\"color: red;\">id already exits.....</h1>");
                requestDispatcher.include(req,resp);
            }
            HttpSession session=req.getSession();
        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }



    }
}
