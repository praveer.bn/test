package com;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.*;
@WebServlet("/Update")
public class UpdateServlet extends HttpServlet {
    Connection connection;
    PreparedStatement preparedStatement;
    Statement statement;
    ResultSet resultSet;
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("text/html");
        PrintWriter printWriter=resp.getWriter();
        int id= Integer.parseInt(req.getParameter("uid"));
        int ph= Integer.parseInt(req.getParameter("uph"));
        try {
            connection = UserConnection.getConnection();
            statement = connection.createStatement();
            resultSet = statement.executeQuery("select * from person where id=" + id);
            if (resultSet.next()){
                preparedStatement = connection.prepareStatement("update person set ph="+ph+" where id="+id);
                preparedStatement.executeUpdate();
                RequestDispatcher requestDispatcher = req.getRequestDispatcher("index.jsp");
                printWriter.print("<h1>User ph updater</h1>");
                requestDispatcher.include(req, resp);
            }else{
                RequestDispatcher requestDispatcher = req.getRequestDispatcher("Delete.jsp");
                printWriter.print("<h1 style=\"color: red;\">ID not present in database</h1>");
                requestDispatcher.include(req, resp);
            }
            HttpSession session=req.getSession();

        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

    }
}
