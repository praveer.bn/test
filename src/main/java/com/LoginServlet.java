package com;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.*;
@WebServlet("/login")
public class LoginServlet extends HttpServlet {
    Connection connection;
    PreparedStatement preparedStatement;
    Statement statement;
    ResultSet resultSet;
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("text/html");
        PrintWriter printWriter=resp.getWriter();
        String email=req.getParameter("uemail");
        int id = Integer.parseInt(req.getParameter("uid"));
        int ph=Integer.parseInt(req.getParameter("uph"));
        printWriter.print("<h1>Welcome  "+email+"</h1>");
        try {
            connection = UserConnection.getConnection();
            preparedStatement = connection.prepareStatement("select * from person where id=" + id);
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
//
                if (resultSet.getString(8).equals("admin")) {
                    statement=connection.createStatement();
               resultSet=statement.executeQuery("select * from person");
               printWriter.print("<h1>Welcome Admin</h1>");
                while (resultSet.next()) {
                    printWriter.println("<center><Table border ='2'> ");
                    printWriter.println("<tr> <th>ID</th>  <th>FNAME</th> <th>LNAME</th> <th>PHONE NUMBER</th> <th>AGE</th> <th>EMAIL</th> <th>CITY</th> <th>ROLE</th> </tr>");
                    printWriter.println("<tr><td>" + resultSet.getInt(1) + "</td><td>" +
                           resultSet.getString(2) + "</td><td>" + resultSet.getString(3) + "</td><td>" + resultSet.getInt(4) + "</td><td>" + resultSet.getInt(5) + "</td><td>" + resultSet.getString(6) + "</td><td>" + resultSet.getString(7) + "</td><td>" + resultSet.getString(8) + "</td></tr>");
                   printWriter.println("</Table></center>");

               }
               printWriter.print("<a href='index.jsp'>Logout</a><br>");
               printWriter.print("<a href='Update.jsp'>Update user phone number</a><br>");
               printWriter.print("<a href='Delete.jsp'>Delete user</a>");
                    HttpSession session=req.getSession();
                    session.invalidate();
                break;

            }
            else if(resultSet.getString(8).equals("user")){
                statement = connection.createStatement();
                resultSet = statement.executeQuery("select * from person where id=" + id);
                if (resultSet.next()) {
                   preparedStatement=connection.prepareStatement("select * from person where id=" + id + " and email='" + email + "' and ph=" + ph);
                    resultSet = preparedStatement.executeQuery();
                    while (resultSet.next()) {
                        printWriter.println("<center><Table border ='2'> ");
                        printWriter.println("<tr> <th>ID</th>  <th>FNAME</th> <th>LNAME</th> <th>PHONE NUMBER</th> <th>AGE</th> <th>EMAIL</th> <th>CITY</th> <th>ROLE</th> </tr>");
                        printWriter.println("<tr><td>" + resultSet.getInt(1) + "</td><td>" +
                                resultSet.getString(2) + "</td><td>" + resultSet.getString(3) + "</td><td>" + resultSet.getInt(4) + "</td><td>" + resultSet.getInt(5) + "</td><td>" + resultSet.getString(6) + "</td><td>" + resultSet.getString(7) + "</td><td>" + resultSet.getString(8) + "</td></tr>");
                        printWriter.println("</Table></center>");

                    }
                    printWriter.print("<a href='index.jsp'>Logout</a>");
                } else {
                    RequestDispatcher requestDispatcher = req.getRequestDispatcher("index.jsp");
                    printWriter.print("<h1 style=\"color: red;\">invalid credentials.....</h1>");
                    requestDispatcher.include(req, resp);

                }
            }
            }

        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

    }
}
